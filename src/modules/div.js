export function div(a, b) {
    if (b === 0) {
      console.log('Cannot divide by zero');
    }
    return a / b;
  }
